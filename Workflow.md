
# Index Page Workflow
In this HTML file, the user can perform 4 functionalities: add, delete, display, and modify.

# Dispatch Page Workflow
Here, the user inputs the drug Id needed for dispatch. Meanwhile, in another component, the cart increments specifying the number of drugs to be dispatched, and after the button is clicked, those drugs are removed from the storage.

To display the drugs available after dispatch, on this page the list of drugs available will be returned alongside the total left.
